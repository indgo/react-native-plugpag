package br.com.indgo;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPag;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagActivationData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagAppIdentification;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagInitializationResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPaymentData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagTransactionResult;

public class PlugpagModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private PlugPag plugPag;

    public PlugpagModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "Plugpag";
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Testing libUpdates: " + numberArgument + " stringArgument: " + stringArgument);
    }

    @ReactMethod
    public void initPlugPagService(String applicationName, String applicationVersion) {
        this.plugPag = new PlugPag(reactContext, new PlugPagAppIdentification(applicationName, applicationVersion));
    }

    @ReactMethod
    public void startPayment(int amount, String saleCode, Promise promise) {
        try {
            PlugPagPaymentData paymentData =
                    new PlugPagPaymentData(
                            PlugPag.TYPE_CREDITO,
                            amount,
                            PlugPag.INSTALLMENT_TYPE_A_VISTA,
                            1,
                            saleCode);

            PlugPagInitializationResult initResult = this.plugPag.initializeAndActivatePinpad(new PlugPagActivationData("403938"));

            if (initResult.getResult() == PlugPag.RET_OK) {
                PlugPagTransactionResult result = this.plugPag.doPayment(paymentData);
                if(result.getResult() == PlugPag.RET_OK){
                    promise.resolve("Inicialização finalizada com êxito.");
                }
                else {
                    promise.reject(result.getErrorCode(), result.getMessage());
                }
            }
            else {
                promise.reject(initResult.getErrorCode(), initResult.getErrorMessage());
            }
        }
        catch (Exception e){
            promise.reject(e);
        }
    }
}
